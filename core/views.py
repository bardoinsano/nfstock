
from contatos.models import Contato
from planos.models import Plano
from django.shortcuts import render
from django.utils.html import escape
from contatos.views import send_email
from django.contrib.auth import get_user_model
from djqscsv import render_to_csv_response
from django.http import HttpResponseRedirect


def home(request):
    if request.method == 'POST':
        nome = escape(request.POST.get('nome'))
        telefone = escape(request.POST.get('telefone'))

        contato = Contato(nome=nome)
        contato.telefone = telefone
        contato.save()

        status = send_email(contato)

        return HttpResponseRedirect('/')

    planos = Plano.objects.all().order_by('valor')

    context = {
        'planos': planos,
    }
    return render(request, 'views/home.html', context)

def checkout(request):
    User = get_user_model()
    if request.user.is_authenticated():
        usuario = request.user

        planos = Plano.objects.all().order_by('valor')
        context = {
            'planos': planos,
        }

        return render(request, 'checkout/checkout.html', context)

    else:
        return HttpResponseRedirect('/login')
"""
    if request.method == 'POST':
        nome = escape(request.POST.get('nome'))
        telefone = escape(request.POST.get('telefone'))

        contato = Contato(nome=nome)
        contato.telefone = telefone
        contato.save()

        status = send_email(contato)

        return HttpResponseRedirect('/')
"""

def admin_export_users(request):
    if request.user.is_authenticated() and request.user.is_superuser:
        contatos = Contato.objects.all().values('pk', 'nome', 'telefone')
        print contatos
        return render_to_csv_response(
            contatos,
            use_verbose_names=True,
            field_header_map={
                'pk': u'ID',
                'nome': 'Nome',
                'telefone': 'Telefone',
            }
        )
    return HttpResponseRedirect('/')