# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.template import Context
from django.conf import settings
from django.template.loader import get_template


def send_email(contato):
    context = {
        'contato': contato,
        'nome': contato.nome,
        'telefone': contato.telefone,
    }
    message = get_template(
        'email/cliente.tpl'
    ).render(context)

    msg = EmailMessage(
        "Solicitação de Contato",
        message, to=[settings.EMAIL_HOST_USER],
        from_email=settings.EMAIL_HOST_USER
    )

    msg.content_subtype = 'html'
    msg.send()
    return True