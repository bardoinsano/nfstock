# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Contato(models.Model):
	nome = models.CharField(max_length=200)
	telefone = models.CharField(max_length=30)

	def __unicode__(self):
		return self.nome