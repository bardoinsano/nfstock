# -*- encoding: utf-8 -*-
from django.contrib import admin
from contatos.models import Contato


class ContatoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'telefone',)

admin.site.register(Contato, ContatoAdmin)