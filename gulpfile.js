var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
let cleanCSS = require('gulp-clean-css');

gulp.task('compile-less', function(done) {
// do stuff
gulp.src(['./media/src/style/**/*.css','./media/src/style/**/*.less']).pipe(concat('estilos.less')).pipe(less()).pipe(cleanCSS()).pipe(gulp.dest('./media/static/css/'));done();});

gulp.task('watch-less', function(done) {
// do stuff
gulp.watch('./media/src/style/*/*.less', gulp.series(['compile-less']));
gulp.watch('./media/src/style/*.less', gulp.series(['compile-less']));
gulp.watch('./media/src/style/*/*.css', gulp.series(['compile-less']));
done();
});

gulp.task('default', gulp.series('watch-less', function(done) {
// do more stuff
done();
}));