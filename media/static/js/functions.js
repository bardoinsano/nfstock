var mobilizer = {
	_menuControl: function(btn, menu) {
		var btn = btn || '.btnMenu';
		var menu = menu || '.header-content-menu';
		$(btn).click(function(){
			$(menu+', '+btn).toggleClass('active');
		})
	},
	init: function(){
		mobilizer._menuControl();
	}
}

$(document).ready(function() {
	mobilizer.init();
})