
""" nfstock URL Configuration

The urlpatterns list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib.auth.views import(
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordChangeView,
    PasswordChangeDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
)
from core import views as core
from accounts import views as accounts

urlpatterns = [
    url(r'^$', core.home, name='home'),
    url(r'^login/$', LoginView.as_view(template_name='accounts/login.html'), name='login'),
    url(r'^logout/$', LogoutView.as_view(next_page=reverse_lazy('login')), name='logout'),
    url(r'^accounts/profile/$', accounts.profile, name='profile'),

    url(r'^cadastro/$', accounts.register, name='register'),

     url(r'^checkout/$', core.checkout, name='checkout'),


    #url(r'^password_change/$',PasswordChangeView.as_view(template_name='accounts/password_change_form.html'),name='password_change'),
    #url(r'^password_change/done/$',PasswordChangeDoneView.as_view(template_name='accounts/password_change_done.html'),name='password_change_done'),
    #url(r'^password_reset/$',PasswordResetView.as_view(template_name='accounts/password_reset_form.html'),name='password_reset'),
    #url(r'^password_reset/done/$',PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),name='password_reset_done'),
	#url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',PasswordResetConfirmView.as_view(template_name='accounts/password_reset_confirm.html'),name='password_reset_confirm'),
	#url(r'^reset/done/$',PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),name='password_reset_complete'),


    url(r'^admin/', admin.site.urls),
    url(r'^admin/contatos/exportar/$', core.admin_export_users, name='admin_export_users'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

