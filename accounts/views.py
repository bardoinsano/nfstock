# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from planos.models import Plano
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from .forms import UserCreateForm
from django.utils.html import escape
from django.http import HttpResponseRedirect

#from django.utils.html import escape
 
def register(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Registro efetuado com sucesso!')
            return HttpResponseRedirect('/login')
 
    else:
        form = UserCreateForm()
 
    return render(request, 'accounts/register.html', {'form': form})

def profile(request):
    User = get_user_model()
    if request.user.is_authenticated():
        usuario = request.user
        context = {
            'usuario': usuario,
        }

        return render(request, 'accounts/profile.html', context)
 
    else:
        return HttpResponseRedirect('/login')