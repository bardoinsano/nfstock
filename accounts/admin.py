# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .models import User
from .forms import UserCreationForm, UserChangeForm

class CustomUserAdmin(UserAdmin):
	add_form = UserCreationForm
	form = UserChangeForm
	model = User
	list_display = ['username', 'email', 'nome', 'empresa','telefone', 'plano', 'is_active']
	list_filter = ('is_active',)
	fieldsets = (
		(None, {'fields': ('email', 'password')}),
		('Informações pessoais', {'fields': ('nome','empresa','telefone')}),
		('Planos', {'fields': ('plano',)}),
		('Permissões', {'fields': ('is_active', 'groups')}),
	)

admin.site.register(User, CustomUserAdmin)