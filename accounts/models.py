# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django import forms
from django.db import models
from planos.models import Plano

class User(AbstractUser):
	nome = models.CharField(max_length=150)
	email = models.EmailField()
	empresa = models.CharField(max_length=150)
	telefone = models.CharField(max_length=30)
	plano = models.ForeignKey(Plano, blank=True, null=True)
	is_active = models.BooleanField(default=True)

	def __unicode__(self):
		return self.nome