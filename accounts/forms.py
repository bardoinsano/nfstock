# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

User = get_user_model()

class UserCreateForm(UserCreationForm):
	nome = forms.CharField(label='Nome', min_length=4, max_length=150)
	email = forms.EmailField(label='E-mail')
	empresa = forms.CharField(label='Empresa', min_length=4, max_length=150)
	telefone = forms.CharField(label='Telefone', max_length=30)

	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2", "nome", "empresa", "telefone")

	def clean_email(self):
		email = self.cleaned_data['email'].lower()
		r = User.objects.filter(email=email)
		if r.count():
			raise  ValidationError("E-mail já registrado")
		return email

	def save(self, commit=True):
		user = super(UserCreateForm, self).save(commit=False)
		user.email = self.cleaned_data["email"]
		user.nome = self.cleaned_data["nome"]
		user.empresa = self.cleaned_data["empresa"]
		user.telefone = self.cleaned_data["telefone"]
		if commit:
			user.save()
		return user

class UserChangeForm(UserChangeForm):
	nome = forms.CharField(label='Nome', min_length=4, max_length=150)
	email = forms.EmailField(label='E-mail')
	empresa = forms.CharField(label='Empresa', min_length=4, max_length=150)
	telefone = forms.CharField(label='Telefone', max_length=30)
	password = ReadOnlyPasswordHashField()

	class Meta:
		model = User
		fields = ("username", "email", "nome", "empresa", "telefone", 'plano', 'is_active')
 
class SignUpForm(UserCreationForm):

	class Meta:
		model = User
		fields = ('username', 'password1')