# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from planos.models import Plano


class PlanoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'descricao', 'valor')

admin.site.register(Plano, PlanoAdmin)
