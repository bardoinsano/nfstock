# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Plano(models.Model):
    nome = models.CharField(max_length=200)
    descricao = models.CharField(max_length=200)
    valor = models.DecimalField(max_digits=6, decimal_places=2)

    def __unicode__(self):
        return self.nome
